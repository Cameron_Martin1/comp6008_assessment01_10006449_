﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog1
{
    public class calc
    {
        public static int calc1(string input)
        {
            int num, three, five, c, answer = 0;
            num = int.Parse(input);

            for (c = 1; c < num; c++)
            {
                three = c % 3;
                five = c % 5;

                if (three == 0 || five == 0)
                {
                    answer = answer + c;
                }
            }
            return answer;
        }
    }
}
