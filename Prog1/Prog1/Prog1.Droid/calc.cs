﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog1
{
    public class instance
    {
        public static int instance1(string input)
        {
            int num, three, five, i, answer = 0;
            num = int.Parse(input);

            for (i = 1; i < num; i++)
            {
                three = i % 3;
                five = i % 5;

                if (three == 0 || five == 0)
                {
                    answer = answer + i;
                }
            }
            return answer;
        }
    }
}
